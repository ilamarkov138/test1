from pydoc import doc
import random
import time

def game(n, playerMove, stonesNumber):
    print(f"Ход: {playerMove}\nКамней в куче: {n}")
    time.sleep(waitTime)
    if n < 1:
        if playerMove % 2 == 0: return print("Ха-Ха проиграл!")
        else: return print("Ха-Ха проиграл!")
    if playerMove % 2 == 0: stonesNumber = inputStones()
    else: stonesNumber = random.randint(1, 4)
    return game(n - stonesNumber, playerMove + 1, 0)

def inputStones():
    stonesNumber = 0
    while (True):
        stonesNumber = int(input("Сколько камней хотите взять?  "))
        if stonesNumber not in range(1, 4):
            print("Введите число от 1 до 3!")
        else:
            break
    return stonesNumber

waitTime = 2
n = random.randint(4, 31)
print(f"Имеется кучище из {n} камней. Каждый игрок берет по очереди 1-3 камня.\n\
Выигрывает тот, кто оставляет последний камень сопернику\n")
time.sleep(waitTime)
playerFirstMove = random.randint(0, 2)
print("Первый ход - {}".format("Игрок" if playerFirstMove == 0 else "Крип-крипочек"))
time.sleep(waitTime)
print("Погналииии")
time.sleep(waitTime)
game(n, playerFirstMove, 0)
